/*=========================================================================
*
*  Copyright Insight Software Consortium
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0.txt
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*
*=========================================================================*/
#ifndef __itkMultipleObjectRegistrationFramework_h
#define __itkMultipleObjectRegistrationFramework_h

#include "itkProcessObject.h"


#include <vector>

namespace itk
{

	/** \class ImageRegistrationMethodv4
	* \brief Interface method for the current registration framework.
	*
	*
	* \author Nick Tustison
	* \author Brian Avants
	*
	* \ingroup ITKRegistrationMethodsv4
	*/
	template<typename TFixedImage, typename TMovingImage, typename TOutputTransform>
	class ITK_EXPORT MultipleObjectRegistrationFramework
		:public ProcessObject
	{
	public:
		/** Standard class typedefs. */
		using Self = MultipleObjectRegistrationFramework;
		using Superclass = ProcessObject;
		using Pointer = SmartPointer<Self>;
		using ConstPointer = SmartPointer<const Self>;

		/** Method for creation through the object factory. */
		itkNewMacro(Self);

		/** ImageDimension constants */
		itkStaticConstMacro(ImageDimension, unsigned int, TFixedImage::ImageDimension);

		/** Run-time type information (and related methods). */
		itkTypeMacro(ImageRegistrationMethodv4, ProcessObject);

		/** Input typedefs for the images and transforms. */
		typedef TFixedImage                                                 FixedImageType;
		typedef typename FixedImageType::Pointer                            FixedImagePointer;
		typedef std::vector<FixedImagePointer>                              FixedImagesContainerType;
		typedef TMovingImage                                                MovingImageType;
		typedef typename MovingImageType::Pointer                           MovingImagePointer;
		typedef std::vector<MovingImagePointer>                             MovingImagesContainerType;

		/** Metric and transform typedefs */
		typedef ObjectToObjectMetricBase                                    MetricType;
		typedef typename MetricType::Pointer                                MetricPointer;

		typedef ObjectToObjectMultiMetricv4<ImageDimension, ImageDimension> MultiMetricType;
		typedef ImageToImageMetricv4<FixedImageType, MovingImageType>       ImageMetricType;
		typedef typename ImageMetricType::VirtualImageType                  VirtualImageType;

		typedef TOutputTransform                                            OutputTransformType;
		typedef typename OutputTransformType::Pointer                       OutputTransformPointer;
		typedef typename OutputTransformType::ScalarType                    RealType;
		typedef typename OutputTransformType::DerivativeType                DerivativeType;
		typedef typename DerivativeType::ValueType                          DerivativeValueType;

		typedef Transform<RealType, ImageDimension, ImageDimension>         InitialTransformType;
		typedef typename InitialTransformType::Pointer                      InitialTransformPointer;

		typedef CompositeTransform<RealType, ImageDimension>                CompositeTransformType;
		typedef typename CompositeTransformType::Pointer                    CompositeTransformPointer;

		/**
		* Type for the output: Using Decorator pattern for enabling the transform to be
		* passed in the data pipeline
		*/
		typedef DataObjectDecorator<OutputTransformType>                    DecoratedOutputTransformType;
		typedef typename DecoratedOutputTransformType::Pointer              DecoratedOutputTransformPointer;

		/** array typedef **/
		typedef Array<SizeValueType>                                        ShrinkFactorsArrayType;
		typedef Array<RealType>                                             SmoothingSigmasArrayType;
		typedef Array<RealType>                                             MetricSamplingPercentageArrayType;

		/** Transform adaptor typedefs */
		typedef TransformParametersAdaptor<OutputTransformType>             TransformParametersAdaptorType;
		typedef typename TransformParametersAdaptorType::Pointer            TransformParametersAdaptorPointer;
		typedef std::vector<TransformParametersAdaptorPointer>              TransformParametersAdaptorsContainerType;

		/**  Type of the optimizer. */
		typedef ObjectToObjectOptimizerBase                                 OptimizerType;
		typedef typename OptimizerType::Pointer                             OptimizerPointer;

		/** enum type for metric sampling strategy */
		enum MetricSamplingStrategyType { NONE, REGULAR, RANDOM };

		typedef typename ImageMetricType::FixedSampledPointSetType          MetricSamplePointSetType;

		/** Set/get the fixed images. */
		virtual void SetFixedImage(const FixedImageType *image)
		{
			this->SetFixedImage(0, image);
		}
		virtual const FixedImageType * GetFixedImage() const
		{
			return this->GetFixedImage(0);
		}
		virtual void SetFixedImage(SizeValueType, const FixedImageType *);
		virtual const FixedImageType * GetFixedImage(SizeValueType) const;

		/** Set the moving images. */
		virtual void SetMovingImage(const MovingImageType *image)
		{
			this->SetMovingImage(0, image);
		}
		virtual const MovingImageType * GetMovingImage() const
		{
			return this->GetMovingImage(0);
		}
		virtual void SetMovingImage(SizeValueType, const MovingImageType *);
		virtual const MovingImageType * GetMovingImage(SizeValueType) const;

		/** Set/Get the optimizer. */
		itkSetObjectMacro(Optimizer, OptimizerType);
		itkGetModifiableObjectMacro(Optimizer, OptimizerType);

		/** Set/Get the metric. */
		itkSetObjectMacro(Metric, MetricType);
		itkGetModifiableObjectMacro(Metric, MetricType);

		/** Set/Get the metric sampling strategy. */
		itkSetMacro(MetricSamplingStrategy, MetricSamplingStrategyType);
		itkGetConstMacro(MetricSamplingStrategy, MetricSamplingStrategyType);

		/** Set the metric sampling percentage. */
		void SetMetricSamplingPercentage(const RealType);

		/** Set the metric sampling percentage. */
		itkSetMacro(MetricSamplingPercentagePerLevel, MetricSamplingPercentageArrayType);
		itkGetConstMacro(MetricSamplingPercentagePerLevel, MetricSamplingPercentageArrayType);

		/** Set/Get the initial fixed transform. */
		itkSetObjectMacro(FixedInitialTransform, InitialTransformType);
		itkGetModifiableObjectMacro(FixedInitialTransform, InitialTransformType);

		/** Set/Get the initial moving transform. */
		itkSetObjectMacro(MovingInitialTransform, InitialTransformType);
		itkGetModifiableObjectMacro(MovingInitialTransform, InitialTransformType);

		/** Set/Get the transform adaptors. */
		void SetTransformParametersAdaptorsPerLevel(TransformParametersAdaptorsContainerType &);
		const TransformParametersAdaptorsContainerType & GetTransformParametersAdaptorsPerLevel() const;

		/**
		* Set/Get the number of multi-resolution levels.  In setting the number of
		* levels we need to set the following for each level:
		*   \li shrink factors for the virtual domain
		*   \li sigma smoothing parameter
		*   \li transform adaptor with specific parameters for the specified level
		*/
		void SetNumberOfLevels(const SizeValueType);
		itkGetConstMacro(NumberOfLevels, SizeValueType);

		/**
		* Set/Get the shrink factors for each level. At each resolution level, the
		* \c itkShrinkImageFilter is called.  For a 256x256 image, a shrink factor
		* of 2 reduces the image to 128x128.
		*/
		itkSetMacro(ShrinkFactorsPerLevel, ShrinkFactorsArrayType);
		itkGetConstMacro(ShrinkFactorsPerLevel, ShrinkFactorsArrayType);

		/**
		* Set/Get the smoothing sigmas for each level.  At each resolution level, a gaussian smoothing
		* filter (specifically, the \c itkDiscreteGaussianImageFilter) is applied.  Sigma values are
		* specified according to the option \c m_SmoothingSigmasAreSpecifiedInPhysicalUnits.
		*/
		itkSetMacro(SmoothingSigmasPerLevel, SmoothingSigmasArrayType);
		itkGetConstMacro(SmoothingSigmasPerLevel, SmoothingSigmasArrayType);

		/**
		* Set/Get whether to specify the smoothing sigmas for each level in physical units
		* (default) or in terms of voxels.
		*/
		itkSetMacro(SmoothingSigmasAreSpecifiedInPhysicalUnits, bool);
		itkGetConstMacro(SmoothingSigmasAreSpecifiedInPhysicalUnits, bool);
		itkBooleanMacro(SmoothingSigmasAreSpecifiedInPhysicalUnits);

		/** Make a DataObject of the correct type to be used as the specified output. */
		typedef ProcessObject::DataObjectPointerArraySizeType DataObjectPointerArraySizeType;
		using Superclass::MakeOutput;
		virtual DataObjectPointer MakeOutput(DataObjectPointerArraySizeType);

		/** Returns the transform resulting from the registration process  */
		virtual const DecoratedOutputTransformType * GetOutput() const;

		/** Get the current level.  This is a helper function for reporting observations. */
		itkGetConstMacro(CurrentLevel, SizeValueType);

		/** Get the current iteration.  This is a helper function for reporting observations. */
		itkGetConstReferenceMacro(CurrentIteration, SizeValueType);

		/* Get the current metric value.  This is a helper function for reporting observations. */
		itkGetConstReferenceMacro(CurrentMetricValue, RealType);

		/** Get the current convergence value.  This is a helper function for reporting observations. */
		itkGetConstReferenceMacro(CurrentConvergenceValue, RealType);

		/** Get the current convergence state per level.  This is a helper function for reporting observations. */
		itkGetConstReferenceMacro(IsConverged, bool);

	protected:
		ImageRegistrationMethodv4();
		virtual ~ImageRegistrationMethodv4();
		virtual void PrintSelf(std::ostream & os, Indent indent) const;

		/** Perform the registration. */
		virtual void  GenerateData();

		/** Initialize by setting the interconnects between the components. */
		virtual void InitializeRegistrationAtEachLevel(const SizeValueType);

		/** Get metric samples. */
		virtual void SetMetricSamplePoints();

		SizeValueType	m_CurrentLevel;
		SizeValueType	m_NumberOfLevels;
		SizeValueType	m_CurrentIteration;
		RealType		m_CurrentMetricValue;
		RealType		m_CurrentConvergenceValue;
		bool			m_IsConverged;

		FixedImagesContainerType                                        m_FixedSmoothImages;
		MovingImagesContainerType                                       m_MovingSmoothImages;
		SizeValueType                                                   m_NumberOfFixedImages;
		SizeValueType                                                   m_NumberOfMovingImages;

		OptimizerPointer                                                m_Optimizer;

		MetricPointer                                                   m_Metric;
		MetricSamplingStrategyType                                      m_MetricSamplingStrategy;
		MetricSamplingPercentageArrayType                               m_MetricSamplingPercentagePerLevel;

		ShrinkFactorsArrayType                                          m_ShrinkFactorsPerLevel;
		SmoothingSigmasArrayType                                        m_SmoothingSigmasPerLevel;
		bool                                                            m_SmoothingSigmasAreSpecifiedInPhysicalUnits;

		InitialTransformPointer                                         m_MovingInitialTransform;
		InitialTransformPointer                                         m_FixedInitialTransform;

		TransformParametersAdaptorsContainerType                        m_TransformParametersAdaptorsPerLevel;

		CompositeTransformPointer                                       m_CompositeTransform;

		OutputTransformPointer                                          m_OutputTransform;

	private:
		ImageRegistrationMethodv4(const Self &);   //purposely not implemented
		void operator=(const Self &);                  //purposely not implemented
	};
} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkMultiObjectRegistrationFramework.hxx"
#endif

#endif
