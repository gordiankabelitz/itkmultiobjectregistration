/*=========================================================================
 *
 *  Copyright Insight Software Consortium
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0.txt
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *=========================================================================*/
 #ifndef itkLabelImageToMultipleImageFilter_h
 #define itkLabelImageToMultipleImageFilter_h

#include "itkImageToImageFilter.h"
#include "itkLabelImageToShapeLabelMapFilter.h"
#include "itkRegionOfInterestImageFilter.h"


namespace itk
{
	template< typename TLabelImage, typename TIntensityImage = TLabelImage, typename TOutputImage = TLabelImage>
	class ITK_TEMPLATE_EXPORT LabelImageToMultipleImageFilter :
	public ImageToImageFilter<TIntensityImage, TOutputImage>
	{
	public:
		ITK_DISALLOW_COPY_AND_ASSIGN(LabelImageToMultipleImageFilter);

		using Self         = LabelImageToMultipleImageFilter;
		using Superclass   = ImageToImageFilter<TLabelImage, TIntensityImage>;
		using Pointer      = SmartPointer<Self>;
		using ConstPointer = SmartPointer<const Self>;

		itkNewMacro(Self);
		itkTypeMacro(LabelImageToMultipleImageFilter, ImageToImageFilter);

		using LabelImageType = TLabelImage;
		using LabelPixelType = typename LabelImageType::PixelType;
		using LabelImagePointer = typename LabelImageType::Pointer ;
		using IntensityImageType = TIntensityImage;
		using IntensityPixelType = typename IntensityImageType::PixelType;
		using IntensityImagePointer = typename IntensityImageType::Pointer;
		using OutputImageType = TOutputImage;
		using OutputImagePointer = typename OutputImageType::Pointer;

		using Labeltype = int;
		using ShapeLabelObjectType = ShapeLabelObject<Labeltype, LabelImageType::ImageDimension>;
		using LabelMapType = LabelMap<ShapeLabelObjectType>;
		using LabelMapTypePointer = LabelMapType*;

		itkGetConstMacro(NumberOfLabels, int);
		itkGetConstMacro(CurrentLabel, int);

		virtual void SetInput(LabelImagePointer InputLabelImage);

		void GenerateOutputInformation() override;

		void GenerateInputRequestedRegion() override;

	protected:

		using LabelImageToShapeLabelMapFilterType    = LabelImageToShapeLabelMapFilter<LabelImageType, LabelMapType>;
		using LabelImageToShapeLabelMapFilterPointer = typename LabelImageToShapeLabelMapFilterType::Pointer;
		using RegionOfInterestImageFilterType        = RegionOfInterestImageFilter<IntensityImageType, OutputImageType>;
		using RegionOfInterestImageFilterPointer     = typename RegionOfInterestImageFilterType::Pointer;

		LabelImageToShapeLabelMapFilterPointer m_LabelImageToShapeLabelMapFilter;
		RegionOfInterestImageFilterPointer     m_RegionOfInterestFilter;

		int m_NumberOfLabels;
		int m_CurrentLabel;
		LabelMapTypePointer m_LabelMapPointer;

		LabelImageToMultipleImageFilter();
		~LabelImageToMultipleImageFilter() override;
		void PrintSelf(std::ostream& os, Indent indent) const override;

		void GenerateData() override;
		void EnlargeOutputRequestedRegion(itk::DataObject *data) override;
		void GenerateOutputRequestedRegion(itk::DataObject *data) override;

		void ComputeLabelMap();

		void WriteImage(LabelImagePointer, int);

	}; // end of class
	
} // end of namespace itk


#ifndef ITK_MANUAL_INSTANTIATION
#include "itkLabelImageToMultipleImageFilter.hxx"
#endif

#endif