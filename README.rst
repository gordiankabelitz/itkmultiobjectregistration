MultiObjectRegistration
=================================

.. |CircleCI| image:: https://circleci.com/gh/InsightSoftwareConsortium/MultiObjectRegistration.svg?style=shield
    :target: https://circleci.com/gh/InsightSoftwareConsortium/MultiObjectRegistration

.. |TravisCI| image:: https://travis-ci.org/InsightSoftwareConsortium/MultiObjectRegistration.svg?branch=master
    :target: https://travis-ci.org/InsightSoftwareConsortium/MultiObjectRegistration

.. |AppVeyor| image:: https://img.shields.io/appveyor/ci/kabcode/multiobjectregistration.svg
    :target: https://ci.appveyor.com/project/kabcode/multiobjectregistration

=========== =========== ===========
   Linux      macOS       Windows
=========== =========== ===========
|CircleCI|  |TravisCI|  |AppVeyor|
=========== =========== ===========

short

long
