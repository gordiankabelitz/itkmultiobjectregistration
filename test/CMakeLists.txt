itk_module_test()

set(MultiObjectRegistrationTests
  itkLabelImageToMultipleImageFilterTest.cxx
  )

CreateTestDriver(MultiObjectRegistration "${MultiObjectRegistration-Test_LIBRARIES}" "${MultiObjectRegistrationTests}")

itk_add_test(NAME itkLabelImageToMultipleImageFilterTest
	COMMAND MultiObjectRegistrationTestDriver itkLabelImageToMultipleImageFilterTest
)
