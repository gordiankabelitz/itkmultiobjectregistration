#include "itkLabelImageToMultipleImageFilter.h"
#include <cstdlib>
#include <itkImageFileWriter.h>

const signed int Dim = 3;
using Pixeltype = float;
using ImageType = itk::Image<Pixeltype, Dim>;
using ImagePointer = ImageType::Pointer;

using LabelImageToMultipleImageFilterType = itk::LabelImageToMultipleImageFilter<ImageType>;
using LabelImagePointer = LabelImageToMultipleImageFilterType::Pointer;

void CreateImage(ImagePointer);

int itkLabelImageToMultipleImageFilterTest(int argc, char*argv[])
{
	auto image = ImageType::New();
	CreateImage(image);
	auto TestImageWriter = itk::ImageFileWriter<ImageType>::New();
	TestImageWriter->SetFileName("TestImage.nrrd");
	TestImageWriter->SetInput(image);
	TestImageWriter->Update();

	auto lok = true;
	auto okay = true;

	//  Read image into filter
	auto LabelImageFilter = LabelImageToMultipleImageFilterType::New();

	if (LabelImageFilter->GetCurrentLabel() != -1)
		lok = false;
	if (LabelImageFilter->GetNumberOfLabels() != 0)
		lok = false;

	LabelImageFilter.Print(std::cout);

	try
	{
		LabelImageFilter->SetInput(image);
	}
	catch (...)
	{
		lok = false;
	}
	LabelImageFilter.Print(std::cout);
	// Compute the labels after input is set
	if (LabelImageFilter->GetNumberOfLabels() != 4)
		lok = false;
	if (LabelImageFilter->GetCurrentLabel() != 0)
		lok = false;

	okay = lok && okay;
	if (!lok)
		std::cout << "Read error" << std::endl;

	// Output check
	lok = true;
	try
	{
		LabelImageFilter->Update();
	}
	catch (itk::ExceptionObject &EO)
	{
		EO.Print(std::cout);
		lok = false;
	}

	okay = lok && okay;
	if (!lok)
		std::cout << "Process error" << std::endl;

	// Write check
	auto FileWriter = itk::ImageFileWriter<ImageType>::New();
	std::string filenamebase("RIO");
	for (auto i = 0; i < LabelImageFilter->GetNumberOfLabels(); ++i)
	{
		auto filename = filenamebase.append(std::to_string(i));
		filename.append(".nrrd");
		FileWriter->SetInput(LabelImageFilter->GetOutput(i));
		FileWriter->SetFileName(filename);
		try
		{
			FileWriter->Update();
		}
		catch (itk::ExceptionObject &EO)
		{
			EO.Print(std::cout);
			lok = false;
		}
	}
	if (!lok)
		std::cout << "Write error" << std::endl;
	okay = lok && okay;

	// Extract single regions
	auto I2L = itk::LabelImageToShapeLabelMapFilter<ImageType>::New();
	I2L->SetInput(image);
	I2L->Update();

	auto ROIFilter = itk::RegionOfInterestImageFilter<ImageType, ImageType>::New();
	ROIFilter->SetInput(image);

	auto RegionWriter = itk::ImageFileWriter<ImageType>::New();
	std::string filename("ROI");

	auto LabelMap = I2L->GetOutput();
	for (auto i = 0; i < LabelMap->GetNumberOfLabelObjects(); ++i)
	{
		auto LabelObject = LabelMap->GetNthLabelObject(i);
		ROIFilter->SetRegionOfInterest(LabelObject->GetBoundingBox());
		RegionWriter->SetInput(ROIFilter->GetOutput());
		char buffer[100];
		sprintf_s(buffer, "Label_%d.nrrd", i);
		RegionWriter->SetFileName(buffer);
		try
		{
			RegionWriter->Update();
		}
		catch (...)
		{
			std::cout << "region writer error" << std::endl;
		}
	}

	
	/*
	auto ROIFilter = itk::RegionOfInterestImageFilter<ImageType, ImageType>::New();
	ROIFilter->SetInput(image);
	ImageType::IndexType Start1;
	Start1[0] = 20;
	Start1[1] = 30;
	Start1[2] = 40;
	ImageType::SizeType Size1;
	Size1[0] = 60;
	Size1[1] = 70;
	Size1[2] = 40;
	ImageType::RegionType Region1(Start1, Size1);
	ROIFilter->SetRegionOfInterest(Region1);

	auto RegionFileWriter = itk::ImageFileWriter<ImageType>::New();
	RegionFileWriter->SetFileName("Region1.nrrd");
	RegionFileWriter->SetInput(ROIFilter->GetOutput());
	try
	{
		RegionFileWriter->Update();
	}
	catch (itk::ExceptionObject &EO)
	{
		EO.Print(std::cout);
		lok = false;
	}
	

	ImageType::IndexType Start2;
	Start2[0] = 100;
	Start2[1] = 115;
	Start2[2] = 115;
	ImageType::SizeType Size2;
	Size2[0] = 30;
	Size2[1] = 45;
	Size2[2] = 55;
	ImageType::RegionType Region2(Start2, Size2);
	ROIFilter->SetRegionOfInterest(Region2);

	RegionFileWriter->SetFileName("Region2.nrrd");
	RegionFileWriter->SetInput(ROIFilter->GetOutput());
	try
	{
		RegionFileWriter->Update();
	}
	catch (itk::ExceptionObject &EO)
	{
		EO.Print(std::cout);
		lok = false;
	}
	*/
	// Final check
	if (okay)
	{
		return EXIT_SUCCESS;
	}
		return EXIT_FAILURE;
}

void
CreateImage
(ImagePointer Image)
{
	ImageType::IndexType idx;
	idx.Fill(0);
	ImageType::SizeType sz;
	sz.Fill(200);

	ImageType::RegionType reg(idx, sz);
	Image->SetRegions(reg);
	Image->Allocate();
	Image->FillBuffer(0);

	for (unsigned int r = 20; r < 80; r++)
	{
		for (unsigned int c = 30; c < 100; c++)
		{
			for (unsigned int d = 40; d < 80; d++)
			{
				ImageType::IndexType pixelIndex;
				pixelIndex[0] = r;
				pixelIndex[1] = c;
				pixelIndex[2] = d;

				Image->SetPixel(pixelIndex, 1);
			}
		}
	}

	// Set pixels in a different square to a different value
	for (unsigned int r = 100; r < 130; r++)
	{
		for (unsigned int c = 115; c < 160; c++)
		{
			for (unsigned int d = 115; d < 170; d++)
			{
				ImageType::IndexType pixelIndex;
				pixelIndex[0] = r;
				pixelIndex[1] = c;
				pixelIndex[2] = d;

				Image->SetPixel(pixelIndex, 2);
			}
		}
	}

	int center[] = {60,70,150};
	auto radius = 20;
	for (unsigned int r = 40; r < 80; r++)
	{
		for (unsigned int c = 50; c < 90; c++)
		{
			for (unsigned int d = 130; d < 170; d++)
			{
				auto r_c = (r - center[0]) * (r - center[0]);
				auto c_c = (c - center[1]) * (c - center[1]);
				auto d_c = (d - center[2]) * (d - center[2]);
				if (sqrt(r_c + c_c + d_c) < radius)
				{
					ImageType::IndexType pixelIndex;
					pixelIndex[0] = r;
					pixelIndex[1] = c;
					pixelIndex[2] = d;

					Image->SetPixel(pixelIndex, 3);
				}
			}
		}
	}
}
